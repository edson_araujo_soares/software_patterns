#include "EventAggregator/Person.h"
#include "EventAggregator/TimeCreated.h"
#include "EventAggregator/PersonCreated.h"
#include "EventAggregator/NumberDestroyed.h"
#include "EventAggregator/TimeCreatedSubscriber.h"
#include "EventAggregator/StandardEventAggregator.h"
#include "EventAggregator/PersonCreatedSubscriber.h"
#include "EventAggregator/NumberDestroyedSubscriber.h"

int main() {

    auto eventAggregator = std::make_shared<StandardEventAggregator>();

    // Subscribe events on the Event Aggregator
    TimeCreatedSubscriber     timeCreatedSubscriber     (eventAggregator);
    PersonCreatedSubscriber   personCreatedSubscriber   (eventAggregator);
    NumberDestroyedSubscriber numberDestroyedSubscriber (eventAggregator);

    // It uses a more complex object on the event handling process.
    Person person("John Mark", 50);

    // Trigger events (it'll be handled by the subscribers)
    eventAggregator->publish(std::make_shared<NumberDestroyed>(100));
    eventAggregator->publish(std::make_shared<PersonCreated>(person));
    eventAggregator->publish(std::make_shared<TimeCreated>("10:30:00"));

    // Unsubscribe events on the Event Aggregator
    eventAggregator->unsubscribe(timeCreatedSubscriber,     TimeCreated::className());
    eventAggregator->unsubscribe(personCreatedSubscriber,   PersonCreated::className());
    eventAggregator->unsubscribe(numberDestroyedSubscriber, NumberDestroyed::className());

    // Trigger events (there should be no more subscribers)
    eventAggregator->publish(std::make_shared<NumberDestroyed>(100));
    eventAggregator->publish(std::make_shared<PersonCreated>(person));
    eventAggregator->publish(std::make_shared<TimeCreated>("10:30:00"));

    return 0;

}
