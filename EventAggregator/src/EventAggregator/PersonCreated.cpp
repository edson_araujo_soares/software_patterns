#include "EventAggregator/PersonCreated.h"

PersonCreated::PersonCreated(const Person & data)
    : _data(data)
{}

Poco::Any PersonCreated::getData()
{
    return _data;
}
