#include <iostream>
#include "EventAggregator/NumberDestroyed.h"
#include "EventAggregator/NumberDestroyedSubscriber.h"

NumberDestroyedSubscriber::NumberDestroyedSubscriber(std::shared_ptr<EventAggregatorInterface> aggregator)
{
    aggregator->subscribe(*this, NumberDestroyed::className());
}

void NumberDestroyedSubscriber::handle(std::shared_ptr<EventInterface> event)
{
    auto data = Poco::AnyCast<int>(event->getData());
    std::cout << "Hey!! The number is " << data << std::endl;
}
