#include <vector>
#include <typeinfo>
#include <cxxabi.h>
#include <algorithm>
#include "EventAggregator/StandardEventAggregator.h"

void StandardEventAggregator::publish(std::shared_ptr<EventInterface> event)
{
    const std::type_info &typeInfo = typeid(*event);
    auto eventClassName            = abi::__cxa_demangle(typeInfo.name(), nullptr, nullptr, nullptr);

    if ( subscriptions.find(eventClassName) == subscriptions.end() )
        return;

    auto subscribers = subscriptions[eventClassName];
    for ( auto const &item : subscribers )
        item->handle(event);
}

void StandardEventAggregator::subscribe(SubscriberInterface & subscriber, const std::string & event)
{
    if ( subscriptions.find(event) == subscriptions.end() )
        subscriptions[event] = Subscribers();

    subscriptions[event].emplace_back(&subscriber);
}

void StandardEventAggregator::unsubscribe(SubscriberInterface & subscriber, const std::string & event)
{
    if ( subscriptions.find(event) == subscriptions.end() )
        return;

    auto position = std::find(
        subscriptions[event].begin(),
        subscriptions[event].end(),
        &subscriber
    );
    subscriptions[event].erase(position);
}