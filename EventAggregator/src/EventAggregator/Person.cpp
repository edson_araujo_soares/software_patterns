#include "EventAggregator/Person.h"

Person::Person(const std::string & name, int age)
    : _name(name),
      _age(age)
{}

int Person::age()
{
    return _age;
}

std::string Person::name()
{
    return _name;
}
