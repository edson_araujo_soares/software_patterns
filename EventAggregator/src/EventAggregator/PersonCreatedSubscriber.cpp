#include <iostream>
#include "EventAggregator/Person.h"
#include "EventAggregator/PersonCreatedSubscriber.h"

PersonCreatedSubscriber::PersonCreatedSubscriber(std::shared_ptr<EventAggregatorInterface> aggregator)
{
    aggregator->subscribe(*this, "PersonCreated");
}

void PersonCreatedSubscriber::handle(std::shared_ptr<EventInterface> event)
{
    auto person = Poco::AnyCast<Person>(event->getData());
    std::cout << "Hey!! I am " << person.name() << " and my age is " << person.age() << "." << std::endl;
}
