#include "EventAggregator/TimeCreated.h"

TimeCreated::TimeCreated(const std::string & data)
    : _data(data)
{}

Poco::Any TimeCreated::getData()
{
    return _data;
}
