#include <iostream>
#include "Poco/Exception.h"
#include "EventAggregator/TimeCreatedSubscriber.h"

TimeCreatedSubscriber::TimeCreatedSubscriber(std::shared_ptr<EventAggregatorInterface> aggregator)
{
    aggregator->subscribe(*this, "TimeCreated");
}

void TimeCreatedSubscriber::handle(std::shared_ptr<EventInterface> event)
{
    std::string data = Poco::AnyCast<std::string>(event->getData());
    std::cout << "Hey!! The time is " << data << std::endl;
}
