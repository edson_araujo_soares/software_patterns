#include "EventAggregator/NumberDestroyed.h"

NumberDestroyed::NumberDestroyed(int data)
    : _data(data)
{}

Poco::Any NumberDestroyed::getData()
{
    return _data;
}
