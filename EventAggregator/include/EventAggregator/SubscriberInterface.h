#ifndef SoftwarePatterns_SubscriberInterface_INCLUDED
#define SoftwarePatterns_SubscriberInterface_INCLUDED

#include <memory>
#include "EventAggregator/EventInterface.h"

class SubscriberInterface
{
public:
    virtual ~SubscriberInterface() = default;
    virtual void handle(std::shared_ptr<EventInterface>) = 0;

};

#endif
