#ifndef SoftwarePatterns_ObjectContextCreated_INCLUDED
#define SoftwarePatterns_ObjectContextCreated_INCLUDED

#include "EventAggregator/Person.h"
#include "EventAggregator/AbstractEvent.h"

class PersonCreated : public AbstractEvent<PersonCreated>
{
public:
    explicit PersonCreated(const Person &);
    ~PersonCreated() override = default;
    Poco::Any getData() override;

private:
    Person _data;

};

#endif
