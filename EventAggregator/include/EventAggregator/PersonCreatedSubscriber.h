#ifndef SoftwarePatterns_ObjectCreatedContextSubscriber_INCLUDED
#define SoftwarePatterns_ObjectCreatedContextSubscriber_INCLUDED

#include <memory>
#include "EventAggregator/EventInterface.h"
#include "EventAggregator/SubscriberInterface.h"
#include "EventAggregator/EventAggregatorInterface.h"

class PersonCreatedSubscriber : public SubscriberInterface
{
public:
    PersonCreatedSubscriber() = default;
    explicit PersonCreatedSubscriber(std::shared_ptr<EventAggregatorInterface>);
    void handle(std::shared_ptr<EventInterface>) override;

};

#endif
