#ifndef SoftwarePatterns_EventInterface_INCLUDED
#define SoftwarePatterns_EventInterface_INCLUDED

#include "Poco/Any.h"

class EventInterface
{
public:
    virtual ~EventInterface() = default;
    virtual Poco::Any getData() = 0;

};

#endif
