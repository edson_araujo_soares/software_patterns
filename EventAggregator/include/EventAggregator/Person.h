#ifndef SoftwarePatterns_Person_INCLUDED
#define SoftwarePatterns_Person_INCLUDED

#include <string>

class Person
{
public:
    Person(const std::string &, int);
    int age();
    std::string name();

private:
    int _age;
    std::string _name;

};

#endif
