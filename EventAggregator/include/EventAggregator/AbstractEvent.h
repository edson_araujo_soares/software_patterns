#ifndef SoftwarePatterns_AbstractEvent_INCLUDED
#define SoftwarePatterns_AbstractEvent_INCLUDED

#include <string>
#include <typeinfo>
#include <cxxabi.h>
#include "EventAggregator/EventInterface.h"

template <typename T>
class ClassNameHandler
{
public:
    static const std::string className()
    {
        const std::type_info &typeInfo = typeid(T);
        std::string className          = abi::__cxa_demangle(typeInfo.name(), nullptr, nullptr, nullptr);

        return className;
    }

};

template <typename T = EventInterface>
class AbstractEvent :
    public EventInterface,
    public ClassNameHandler<T>
{
public:
    ~AbstractEvent() override = default;

};

#endif
