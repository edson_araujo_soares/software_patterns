#ifndef SoftwarePatterns_NumberDestroyed_INCLUDED
#define SoftwarePatterns_NumberDestroyed_INCLUDED

#include "EventAggregator/AbstractEvent.h"

class NumberDestroyed : public AbstractEvent<NumberDestroyed>
{
public:
    explicit NumberDestroyed(int);
    ~NumberDestroyed() override = default;
    Poco::Any getData() override;

private:
    int _data;

};

#endif
