#ifndef SoftwarePatterns_TimeCreated_INCLUDED
#define SoftwarePatterns_TimeCreated_INCLUDED

#include <string>
#include "EventAggregator/AbstractEvent.h"

class TimeCreated : public AbstractEvent<TimeCreated>
{
public:
    explicit TimeCreated(const std::string &);
    ~TimeCreated() override = default;
    Poco::Any getData() override;

private:
    std::string _data;

};

#endif
