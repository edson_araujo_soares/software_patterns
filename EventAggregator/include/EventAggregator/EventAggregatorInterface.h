#ifndef SoftwarePatterns_EventAggregatorInterface_INCLUDED
#define SoftwarePatterns_EventAggregatorInterface_INCLUDED

#include <memory>
#include <string>
#include "EventAggregator/EventInterface.h"
#include "EventAggregator/SubscriberInterface.h"

class EventAggregatorInterface
{
public:
    virtual ~EventAggregatorInterface() = default;
    virtual void publish(std::shared_ptr<EventInterface>) = 0;
    virtual void subscribe(SubscriberInterface &, const std::string & event) = 0;
    virtual void unsubscribe(SubscriberInterface &, const std::string & event) = 0;

};

#endif
