#ifndef SoftwarePatterns_TimeCreatedSubscriber_INCLUDED
#define SoftwarePatterns_TimeCreatedSubscriber_INCLUDED

#include <memory>
#include "EventAggregator/EventInterface.h"
#include "EventAggregator/SubscriberInterface.h"
#include "EventAggregator/EventAggregatorInterface.h"

class TimeCreatedSubscriber : public SubscriberInterface
{
public:
    TimeCreatedSubscriber() = default;
    explicit TimeCreatedSubscriber(std::shared_ptr<EventAggregatorInterface>);
    void handle(std::shared_ptr<EventInterface>) override;

};

#endif
