#ifndef SoftwarePatterns_NumberDestroyedSubscriber_INCLUDED
#define SoftwarePatterns_NumberDestroyedSubscriber_INCLUDED

#include <memory>
#include "EventAggregator/EventInterface.h"
#include "EventAggregator/SubscriberInterface.h"
#include "EventAggregator/EventAggregatorInterface.h"

class NumberDestroyedSubscriber : public SubscriberInterface
{
public:
    NumberDestroyedSubscriber() = default;
    explicit NumberDestroyedSubscriber(std::shared_ptr<EventAggregatorInterface>);
    void handle(std::shared_ptr<EventInterface>) override;

};

#endif
