#ifndef SoftwarePatterns_StandardEventAggregator_INCLUDED
#define SoftwarePatterns_StandardEventAggregator_INCLUDED

#include <map>
#include <vector>
#include <memory>
#include <string>
#include "EventAggregator/EventAggregatorInterface.h"

class StandardEventAggregator : public EventAggregatorInterface
{
public:
    ~StandardEventAggregator() override = default;
    void publish(std::shared_ptr<EventInterface>) final;
    void subscribe(SubscriberInterface &, const std::string & event) final;
    void unsubscribe(SubscriberInterface &, const std::string & event) final;

private:
    typedef std::vector<SubscriberInterface *> Subscribers;
    std::map<std::string, Subscribers> subscriptions;

};

#endif
